import { Scene, Mesh, BufferGeometry, Vector3, Object3D, Color, Material, MeshPhongMaterial, DoubleSide, Geometry, LineSegments, BoxBufferGeometry } from "three";
import { Util } from "./utils";
import { AppComponent } from "../app.component";
import { GeometryManager } from "./geometry.manager";
import { MaterialManager } from "./material.manager";
import { GeometryInfo, Printing2DLine, ViewType, LineType, Printing2DGeometry, Printing2DGeometryType } from './models';
import { CONFIG as env, GEOMETRY_TYPE } from '../app.config';

export class GroundManager {
    private scene: Scene;
    private APP: AppComponent;
    private utils: Util;

    private geometryManager: GeometryManager;
//Tạo phôi
    public geo_ground: GeometryInfo;
 //Material
    private material: Material;
    private geoGroundHeight: number = 100;
    private geoGroundWidth: number = 10000;
    private geoGroundLength: number = 10000;
    constructor() {
        this.utils = new Util();
        this.geometryManager = GeometryManager.Instance();
    }

    public init(app: AppComponent): Promise<void> {
        return new Promise((resolve, reject) => {
            this.APP = app;
            this.scene = app.scene;
           // this.material = MaterialManager.Instance().DEFAULT.clone();
           this.material = new MeshPhongMaterial({color:new Color('Yellow')});
           this.APP.sldEaveWidth.addAction(this.update.bind(this));
           this.APP.sldExistingWallHeight.addAction(this.update.bind(this));
           this.APP.sldExistingLength.addAction(this.update.bind(this));
            resolve();
        });
    }
    public optimize() : Promise<void> {
        return new Promise((resolve, reject) => {
            this.geo_ground = new GeometryInfo();
            this.geo_ground.geometry = new BoxBufferGeometry(this.geoGroundWidth,this.geoGroundHeight,this.geoGroundLength);

            // .clone()
            // .rotateY(Math.PI / 2)
            // .translate(0, translateHeight, 0);
            // this.geo_ground.width = this.geometryManager.EXISTING_WALL.EXISTING_WALL.width;
            // this.geo_ground.length = this.geometryManager.EXISTING_WALL.EXISTING_WALL.length;
            // this.geo_ground.height = this.geometryManager.EXISTING_WALL.EXISTING_WALL.height;

            resolve();
        });
    }
    public load(): Promise<void> {
        return new Promise((resolve, reject) => {
            this.scene.remove(...this.scene.children.filter(x=> x.userData.type== 'GROUND'));
            // this.scene.remove(...this.scene.children.filter(x => x.userData.type == GEOMETRY_TYPE.EXISTING_WALL));
            // this.scene.remove(...this.scene.children.filter(x => x.userData.type == "EXISTING_WALL_OUTLINE"));
         
            let width = this.APP.sldEaveWidth.currentValue;
            let height = this.APP.sldExistingWallHeight.currentValue;
            let length = this.APP.sldExistingLength.currentValue;
            let ground = new Mesh(this.geo_ground.geometry,this.material);

            ground.userData = {type:'GROUND'};
            ground.scale.set((width)/this.geoGroundWidth,1,(length)/this.geoGroundLength);    
           //this.scene.add(ground);        
            resolve();
        });
    }
    private update(preVal:number,curVal:number){
        this.load();
    }
  
    public addEaveWall(userDataPos: any) {
        
    }

    public uiChanged(preVal: number, curVal): void {
        this.load();
    }
}