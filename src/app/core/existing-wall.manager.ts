import { Scene, Mesh, BufferGeometry, Vector3, Object3D, Color, Material, MeshPhongMaterial, DoubleSide, Geometry, LineSegments, BoxBufferGeometry } from "three";
import { Util } from "./utils";
import { AppComponent } from "../app.component";
import { GeometryManager } from "./geometry.manager";
import { MaterialManager } from "./material.manager";
import { GeometryInfo, Printing2DLine, ViewType, LineType, Printing2DGeometry, Printing2DGeometryType } from './models';
import { CONFIG as env, GEOMETRY_TYPE } from '../app.config';

export class GroundManager {
    private scene: Scene;
    private APP: AppComponent;
    
    private utils: Util;

    private geometryManager: GeometryManager;

    //Phoi
    public geo_ground: GeometryInfo;
    //Material
    private material: Material;

    constructor() {
        this.utils = new Util();
        this.geometryManager = GeometryManager.Instance();

        
    }

    public init(app: AppComponent): Promise<void> {
        return new Promise((resolve, reject) => {
            this.APP = app;
            this.scene = app.scene;
            //this.material = MaterialManager.Instance().DEFAULT.clone();

            this.material = new MeshPhongMaterial({ color: new Color('green') });

            this.APP.sldEaveWidth.addAction(this.update.bind(this));

            resolve();
        });
    }
    public optimize() : Promise<void> {
        return new Promise((resolve, reject) => {
            this.geo_ground = new GeometryInfo();
            this.geo_ground.geometry = new BoxBufferGeometry(10000, 1000, 1000);
            
            // .rotateY(Math.PI / 2)
            // .translate(0, translateHeight, 0);
            // this.geo_existingWallW1.width = this.geometryManager.EXISTING_WALL.EXISTING_WALL.width;
            // this.geo_existingWallW1.length = this.geometryManager.EXISTING_WALL.EXISTING_WALL.length;
            // this.geo_existingWallW1.height = this.geometryManager.EXISTING_WALL.EXISTING_WALL.height;

            resolve();
        });
    }
    public load(): Promise<void> {
        return new Promise((resolve, reject) => {
            this.scene.remove(...this.scene.children.filter(x => x.userData.tyoe == 'COLUMN'));
            // // this.scene.remove(...this.scene.children.filter(x => x.userData.type == "EXISTING_WALL_OUTLINE"));

            
            let column1 = new Mesh(this.geo_ground.geometry, this.material);
            column1.userData = { type: 'COLUMN'} ;
            column1.scale.set(this.APP.sldEaveWidth.currentValue/1000, 1, 1);
            
            this.scene.add(column1);
            resolve();
        });
    }

    private update(preVal: number, curVal: number){
        this.load();
    }
  
    public addEaveWall(userDataPos: any) {
        
    }

    public uiChanged(preVal: number, curVal): void {
        this.load();
    }
}