import { Scene, Mesh, BufferGeometry, Vector3, Object3D, Color, Material, MeshPhongMaterial, DoubleSide, Geometry, LineSegments, BoxBufferGeometry, KeyframeTrack } from "three";
import { Util } from "./utils";
import { AppComponent } from "../app.component";
import { GeometryManager } from "./geometry.manager";
import { MaterialManager } from "./material.manager";
import { GeometryInfo, Printing2DLine, ViewType, LineType, Printing2DGeometry, Printing2DGeometryType } from './models';
import { CONFIG as env, GEOMETRY_TYPE } from '../app.config';

export class KneeManager {
    private scene: Scene;
    private APP: AppComponent;
    private utils: Util;

    private geometryManager: GeometryManager;
//Tạo phôi
    public geo_knee: GeometryInfo;
 //Material
    private material: Material;

    constructor() {
        this.utils = new Util();
        this.geometryManager = GeometryManager.Instance();
    }

    public init(app: AppComponent): Promise<void> {
        return new Promise((resolve, reject) => {
            this.APP = app;
            this.scene = app.scene;
           // this.material = MaterialManager.Instance().DEFAULT.clone();
           this.material = new MeshPhongMaterial({color:new Color('Red')});
           this.APP.sldEaveWidth.addAction(this.update.bind(this));
           this.APP.sldExistingWallHeight.addAction(this.update.bind(this));
           this.APP.sldExistingLength.addAction(this.update.bind(this));
           this.APP.sldExistingPitch.addAction(this.update.bind(this));
            resolve();
        });
    }
    public optimize() : Promise<void> {
        return new Promise((resolve, reject) => {
            //knee
            this.geo_knee = new GeometryInfo();
            this.geo_knee.geometry = this.geometryManager.KNEE.CPDKP15007_right.geometry.clone();
            this.geo_knee.width = this.geometryManager.KNEE.CPDKP15007_right.width;
            this.geo_knee.length = this.geometryManager.KNEE.CPDKP15007_right.length;
            this.geo_knee.height = this.geometryManager.KNEE.CPDKP15007_right.height;
            this.geo_knee.geometry.translate(-this.geo_knee.width/2,0,0);
            this.geo_knee.geometry.rotateX(Math.PI);
            this.geo_knee.geometry.rotateZ(Math.PI);            
        //    console.log('LENGTH', this.geo_rafter);
            resolve();
        });
    }
    public load(): Promise<void> {
        return new Promise((resolve, reject) => {
            this.scene.remove(...this.scene.children.filter(x=> x.userData.type== 'knee of Roof'));
            
            let width = this.APP.sldEaveWidth.currentValue/2;
            let height = this.APP.sldExistingWallHeight.currentValue;
            let length = this.APP.sldExistingLength.currentValue/2;
            let pitch = this.APP.sldExistingPitch.currentValue;

           

            let soluongknee = this.soluongo(length*2);
            let baySize = length*2/soluongknee;
            let offsetZ = -length;
            for(let i=0; i <= soluongknee; i++){  
                this.addKnee(offsetZ,width);
                offsetZ += baySize;
            }

            resolve();
        });
    }
    private ConvertToRad(degree:number):number
    {
        return (degree/180)*Math.PI;
    }
    private ChieuCaoRoof(degree:number,canhday:number):number
    {
        let goc = this.ConvertToRad(degree);
        return(canhday*Math.tan(goc));
    }
    private update(preVal:number,curVal:number){
        this.load();
    }
    public uiChanged(preVal: number, curVal): void {
        this.load();
    }
    private soluongo(X:number):number{
        let soluong = 1;
        while(soluong*2000<X)
            soluong++;
        return soluong;
    }
    public addKnee(offsetX: any, X: any) {
        let knee1 = new Mesh(this.geo_knee.geometry, this.material);
        knee1.userData = {type:'knee of Roof' };  
        knee1.translateX(-X);
        knee1.translateY(this.APP.sldExistingWallHeight.currentValue);
        knee1.translateZ(offsetX);
        knee1.rotateZ(this.ConvertToRad(this.APP.sldExistingPitch.currentValue));
        this.scene.add(knee1);
    }
}