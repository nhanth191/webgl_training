import { Scene, Mesh, BufferGeometry, Vector3, Object3D, Color, Material, MeshPhongMaterial, DoubleSide, Geometry, LineSegments, BoxBufferGeometry } from "three";
import { Util } from "./utils";
import { AppComponent } from "../app.component";
import { GeometryManager } from "./geometry.manager";
import { MaterialManager } from "./material.manager";
import { GeometryInfo, Printing2DLine, ViewType, LineType, Printing2DGeometry, Printing2DGeometryType } from './models';
import { CONFIG as env, GEOMETRY_TYPE } from '../app.config';

export class ColumRafterManager {
    private scene: Scene;
    private APP: AppComponent;
    private utils: Util;

    private geometryManager: GeometryManager;
//Tạo phôi
    public geo_colum: GeometryInfo;
    public geo_purlin: GeometryInfo;
 //Material
    private material: Material;

    constructor() {
        this.utils = new Util();
        this.geometryManager = GeometryManager.Instance();
    }

    public init(app: AppComponent): Promise<void> {
        return new Promise((resolve, reject) => {
            this.APP = app;
            this.scene = app.scene;
           // this.material = MaterialManager.Instance().DEFAULT.clone();
           this.material = new MeshPhongMaterial({color:new Color('Red')});
           this.APP.sldEaveWidth.addAction(this.update.bind(this));
           this.APP.sldExistingWallHeight.addAction(this.update.bind(this));
           this.APP.sldExistingLength.addAction(this.update.bind(this));
           this.APP.sldExistingPitch.addAction(this.update.bind(this));
            resolve();
        });
    }
    public optimize() : Promise<void> {
        return new Promise((resolve, reject) => {
            //Rafter
            this.geo_colum = new GeometryInfo();
            this.geo_colum.geometry = this.geometryManager.COLUMN.S63x3.geometry.clone();
            this.geo_colum.width = this.geometryManager.COLUMN.S63x3.width;
            this.geo_colum.length = this.geometryManager.COLUMN.S63x3.length;
            this.geo_colum.height = this.geometryManager.COLUMN.S63x3.height;
            this.geo_colum.geometry.rotateX(Math.PI/2);
            this.geo_colum.geometry.translate(0, this.geometryManager.COLUMN.S63x3.length/2, 0);
        //    console.log('LENGTH', this.geo_rafter);
            resolve();
        });
    }
    public load(): Promise<void> {
        return new Promise((resolve, reject) => {
            this.scene.remove(...this.scene.children.filter(x=> x.userData.type== 'Colum of Roof'));
            
            let width = this.APP.sldEaveWidth.currentValue/2;
            let height = this.APP.sldExistingWallHeight.currentValue;
            let length = this.APP.sldExistingLength.currentValue/2;
            let pitch = this.APP.sldExistingPitch.currentValue;

            let colum1 = new Mesh(this.geo_colum.geometry, this.material);
            colum1.userData = {type:'Colum of Roof' };           
            colum1.translateZ(+length);
            colum1.translateY(height);
            colum1.scale.set(1, this.ChieuCaoRoof(pitch,width)/this.geo_colum.length, 1);
            this.scene.add(colum1);

            let colum2 = new Mesh(this.geo_colum.geometry, this.material);
            colum2.userData = {type:'Colum of Roof' };           
            colum2.translateZ(-length);
            colum2.translateY(height);
            colum2.scale.set(1, this.ChieuCaoRoof(pitch,width)/this.geo_colum.length, 1);
             this.scene.add(colum2);
            resolve();
        });
    }
    private ConvertToRad(degree:number):number
    {
        return (degree/180)*Math.PI;
    }
    private ChieuCaoRoof(degree:number,canhday:number):number
    {
        let goc = this.ConvertToRad(degree);
        return(canhday*Math.tan(goc));
    }
    private update(preVal:number,curVal:number){
        this.load();
    }
  
    public addRafter(offsetX: any, X: any) {
        
    }
    public uiChanged(preVal: number, curVal): void {
        this.load();
    }
}